DROP DATABASE IF EXISTS EnjoyTheLine;
CREATE DATABASE IF NOT EXISTS EnjoyTheLine;

USE EnjoyTheLine;

/*Table structure for table person */
DROP TABLE IF EXISTS person;
CREATE TABLE person (
    person_ID INTEGER AUTO_INCREMENT NOT NULL,
    firstName VARCHAR(40) NOT NULL,
    lastName VARCHAR(30) NOT NULL,
	postalCode VARCHAR(10) NOT NULL,
    emailAddress VARCHAR(30) NOT NULL,
    phone VARCHAR(24) NOT NULL,
    PRIMARY KEY (person_ID),
    UNIQUE KEY (emailAddress)
);

/*Table structure for table person */
DROP TABLE IF EXISTS store;
CREATE TABLE store (
    store_ID INTEGER AUTO_INCREMENT NOT NULL,
    storeName VARCHAR(40) NOT NULL,
    maxCapacity INTEGER NOT NULL,
    PRIMARY KEY (store_ID)
);

/*Table structure for table person */
DROP TABLE IF EXISTS queue;
CREATE TABLE queue (
	queue_ID INTEGER AUTO_INCREMENT NOT NULL,
    store_ID INTEGER NOT NULL,
    person_ID INTEGER NOT NULL,
    admitted BOOLEAN NOT NULL,
    joined_time DATETIME DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (queue_ID),
    FOREIGN KEY (store_ID)
        REFERENCES store (store_ID),
    FOREIGN KEY (person_ID)
        REFERENCES person (person_ID)    
);


insert into store (store_ID,storeName,maxCapacity) values (1,'Tesco', 10);
insert into store (store_ID,storeName,maxCapacity) values (2,'Primark', 4);

select * from person;

select * from store;

select * from queue;
