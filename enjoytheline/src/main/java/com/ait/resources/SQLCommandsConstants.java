package com.ait.resources;

public class SQLCommandsConstants {

	final public static String PERSON_INSERT = "INSERT INTO person(firstName,lastName,postalCode,emailAddress,phone) VALUES (?,?,?,?,?)";
	final public static String PERSON_SELECT = "SELECT person_ID,firstName,lastName,postalCode,emailAddress,phone FROM person WHERE person_ID = ?";
	final public static String PERSON_SELECT_BY_NAME = "SELECT person_ID,firstName,lastName,postalCode,emailAddress,phone FROM person WHERE firstName = ? AND lastName = ?";
	final public static String PERSON_SELECT_BY_EMAIL = "SELECT person_ID,firstName,lastName,postalCode,emailAddress,phone FROM person WHERE emailAddress = ?";

	final public static String PERSON_SELECT_ALL = "SELECT person_ID,firstName,lastName,postalCode,emailAddress,phone FROM person";

	final public static String STORE_SELECT_ALL = "SELECT store_ID,storeName,maxCapacity FROM store";
	final public static String STORE_SELECT = "SELECT store_ID,storeName,maxCapacity  FROM store WHERE store_ID = ?";
	final public static String STORE_UPDATE = "UPDATE store SET maxCapacity = ? WHERE store_ID = ?;";

	final public static String STORE_QUEUE_INSERT = "INSERT INTO queue(store_ID,person_ID,admitted) VALUES (?,?,?)";
	final public static String STORE_QUEUE_DELETE = "DELETE FROM queue WHERE queue_ID = ?";

	final public static String STORE_QUEUE_SELECT_BY_NAME = "SELECT queue_ID, store_ID,person_ID,admitted  FROM queue WHERE  person_ID = ? ";

	final public static String STORE_QUEUE_SELECT_NEXT_CUSTOMER = "SELECT queue_ID, q.store_id,p.person_ID,admitted  FROM queue q\n"
																+ "INNER JOIN  person p ON  p.person_ID = q.person_ID\n"
																+ "WHERE queue_ID = (SELECT min(queue_ID)  FROM queue WHERE store_id = ? AND admitted = false); ";

	final public static String STORE_QUEUE_UPDATE_ADMITTED = "UPDATE queue SET admitted = true WHERE queue_ID = ?";

	final public static String STORE_QUEUE_SELECT_ALL_BY_STORE = "SELECT queue_ID, q.store_id, p.person_ID,admitted FROM queue q\n" 
																+ "INNER join person p ON p.person_ID = q.person_ID WHERE store_id = ? AND admitted = false; ";
	
	final public static String STORE_QUEUE_DELETE_OLD = "DELETE FROM queue WHERE store_id = ? AND joined_time >= DATE(NOW()) - INTERVAL 5 DAY;" ;
	
	
}
