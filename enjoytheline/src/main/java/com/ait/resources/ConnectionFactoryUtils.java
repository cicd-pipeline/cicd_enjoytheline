package com.ait.resources;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectionFactoryUtils {
	// init database constants
	// private static final String DATABASE_URL =
	// "jdbc:mysql://localhost:3306/Restaurant?useSSL=false";
	private static final String DATABASE_URL = "jdbc:mysql://localhost:3307/EnjoyTheLine?useSSL=false&allowPublicKeyRetrieval=true";

	private static final String USERNAME = "root";
	private static final String PASSWORD = "admin123";

	// init connection object
	private static Connection connection;
	// init properties object
	private static Properties properties;

	// create properties
	private static Properties getProperties() {
		if (properties == null) {
			properties = new Properties();
			properties.setProperty("user", USERNAME);
			properties.setProperty("password", PASSWORD);
		}
		return properties;
	}

	public static Connection createConnetion() throws ClassNotFoundException {
    		 if (connection == null) {
    	            try {
    	            	DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver ());
    	                connection = DriverManager.getConnection(DATABASE_URL, getProperties());
    	            } catch (SQLException e) {
    	                e.printStackTrace();
    	            }
    	      }
    	  return connection;
    }

	// disconnect database
	public static void disconnect() {
		if (connection != null) { 
			try {
				connection.close();
				connection = null;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}