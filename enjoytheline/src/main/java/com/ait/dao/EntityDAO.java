package com.ait.dao;

import java.sql.SQLException; 

public interface EntityDAO<E> {
	
	E add(E entity) throws SQLException;
	E get(int idEntity) throws SQLException;


}