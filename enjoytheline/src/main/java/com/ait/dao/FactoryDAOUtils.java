package com.ait.dao;

import com.ait.entities.Person;
import com.ait.entities.Store;
import com.ait.entities.StoreQueue;

public class FactoryDAOUtils {
	
	 static EntityDAO<Person> personDAO;
	 static EntityDAO<Store> storeDAO; 
	 static EntityDAO<StoreQueue> storeQueueDAO; 
	 
	 public static PersonDAO getPersonDAO() {
		 if(personDAO == null) {
			 personDAO = new PersonDAO();	  
		 }
		 return (PersonDAO) personDAO;
		 
	 }
	 
	 public static StoreDAO getStoreDAO() {
		 if(storeDAO == null) {
			 storeDAO = new StoreDAO();	  
		 }
		 return (StoreDAO) storeDAO;
		 
	 }
	 
	 public static StoreQueueDAO getStoreQueueDAO() {
		 if(storeQueueDAO == null) {
			 storeQueueDAO = new StoreQueueDAO();	  
		 }
		 return (StoreQueueDAO) storeQueueDAO;
		 
	 }

}
