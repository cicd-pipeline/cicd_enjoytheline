package com.ait.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.ait.entities.Person;
import com.ait.resources.ConnectionFactoryUtils;
import com.ait.resources.SQLCommandsConstants;

public class PersonDAO implements EntityDAO<Person> {

	@Override
	public Person add(final Person person) throws SQLException {
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		try {
			conn = ConnectionFactoryUtils.createConnetion();
			preparedStatement = conn.prepareStatement(SQLCommandsConstants.PERSON_INSERT);
			preparedStatement.setString(1, person.getFirstName());
			preparedStatement.setString(2, person.getLastName());
			preparedStatement.setString(3, person.getPostalCode());
			preparedStatement.setString(4, person.getEmail());
			preparedStatement.setString(5, person.getPhone());
			preparedStatement.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				preparedStatement.close();
				ConnectionFactoryUtils.disconnect();
				conn.close();
			} catch (SQLException e) {
				System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
			}

		}

		return getByEmail(person.getEmail());

	}

	public Person getByEmail(final String email) throws SQLException {

		Connection conn = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Person person = null;
		try {
			conn = ConnectionFactoryUtils.createConnetion();
			preparedStatement = conn.prepareStatement(SQLCommandsConstants.PERSON_SELECT_BY_EMAIL);
			preparedStatement.setString(1, email);
			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				person = new Person(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3),
						resultSet.getString(4), resultSet.getString(5), resultSet.getString(6));

			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				resultSet.close();
				preparedStatement.close();
				ConnectionFactoryUtils.disconnect();
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return person;
	}

	@Override
	public Person get(final int idPerson) throws SQLException {

		Connection conn = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Person person = null;
		try {
			conn = ConnectionFactoryUtils.createConnetion();
			preparedStatement = conn.prepareStatement(SQLCommandsConstants.PERSON_SELECT);
			preparedStatement.setInt(1, idPerson);
			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				person = new Person(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3),
						resultSet.getString(4), resultSet.getString(5), resultSet.getString(6));

			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				resultSet.close();
				preparedStatement.close();
				ConnectionFactoryUtils.disconnect();
				conn.close();
			} catch (SQLException e) {
				System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
			}

		}
		return person;
	}

}
