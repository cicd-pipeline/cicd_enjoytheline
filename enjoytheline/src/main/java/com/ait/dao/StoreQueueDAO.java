package com.ait.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.ait.entities.Person;
import com.ait.entities.Store;
import com.ait.entities.StoreQueue;
import com.ait.resources.ConnectionFactoryUtils;
import com.ait.resources.SQLCommandsConstants;

public class StoreQueueDAO implements EntityDAO<StoreQueue>{

	public void add(final Store store , final Person person) throws SQLException{
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		try {
			conn = ConnectionFactoryUtils.createConnetion();
			preparedStatement = conn.prepareStatement(SQLCommandsConstants.STORE_QUEUE_INSERT);
			preparedStatement.setInt(1, store.getIdStore());
			preparedStatement.setInt(2, person.getIdPerson());
			preparedStatement.setBoolean(3, Boolean.FALSE);
			preparedStatement.executeUpdate();
 
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				preparedStatement.close();
				ConnectionFactoryUtils.disconnect(); 
				conn.close();
			}catch (SQLException e) { 
				System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
			}
			
		}
		
	}

	public List<StoreQueue> getByName(final Person person) throws SQLException {
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet=null;
		final List<StoreQueue> queueList = new ArrayList<StoreQueue>();
		try {
			conn = ConnectionFactoryUtils.createConnetion();
			preparedStatement = conn.prepareStatement(SQLCommandsConstants.STORE_QUEUE_SELECT_BY_NAME);
			preparedStatement.setInt(1, person.getIdPerson());
			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				final StoreQueue queue = new StoreQueue(resultSet.getInt(1),resultSet.getInt(2), resultSet.getInt(3), resultSet.getBoolean(4)); 
				queueList.add(queue);
			}
		
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				resultSet.close();
				preparedStatement.close();
				ConnectionFactoryUtils.disconnect();
				conn.close();
			}catch (SQLException e) {
				System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
			}
			
		}
		
		return queueList;
	}
	
	public List<StoreQueue> getByStore(final Store store)  throws SQLException{
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet=null;
		final List<StoreQueue> queueList = new ArrayList<StoreQueue>();
		try {
			conn = ConnectionFactoryUtils.createConnetion();
			preparedStatement = conn.prepareStatement(SQLCommandsConstants.STORE_QUEUE_SELECT_ALL_BY_STORE);
			preparedStatement.setInt(1, store.getIdStore());
			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				final StoreQueue queue = new StoreQueue(resultSet.getInt(1),resultSet.getInt(2), resultSet.getInt(3), resultSet.getBoolean(4)); 
				queueList.add(queue);
			}
		
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				resultSet.close();
				preparedStatement.close();
				ConnectionFactoryUtils.disconnect();
				conn.close();
			}catch (SQLException e) {
				System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage()); 
			}
			
		}
		
		return queueList;
	}


	public void delete(final int idStoreQueue)  throws SQLException{
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		try {
			conn = ConnectionFactoryUtils.createConnetion();
			preparedStatement = conn.prepareStatement(SQLCommandsConstants.STORE_QUEUE_DELETE);
			preparedStatement.setInt(1, idStoreQueue);
			preparedStatement.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				preparedStatement.close();
				ConnectionFactoryUtils.disconnect();
				conn.close();
			}catch (SQLException e) {
				System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
			}
			
		}
	
	}
	
	public void deleteOldValues(final int idStoreQueue) throws SQLException {
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		try {
			conn = ConnectionFactoryUtils.createConnetion();
			preparedStatement = conn.prepareStatement(SQLCommandsConstants.STORE_QUEUE_DELETE_OLD);
			preparedStatement.setInt(1, idStoreQueue);
			preparedStatement.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				preparedStatement.close();
				ConnectionFactoryUtils.disconnect();
				conn.close();
			}catch (SQLException e) {
				System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
			}
			
		}
	
	}
	
	public StoreQueue  admitNextCustomer(final Store store ) throws SQLException {
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet=null;
		StoreQueue storeQueue = null;
		try {
			conn = ConnectionFactoryUtils.createConnetion();
			preparedStatement = conn.prepareStatement(SQLCommandsConstants.STORE_QUEUE_SELECT_NEXT_CUSTOMER);
			preparedStatement.setInt(1, store.getIdStore());

			resultSet = preparedStatement.executeQuery(); 

			while (resultSet.next()) {
				storeQueue = new StoreQueue(resultSet.getInt(1), resultSet.getInt(2), resultSet.getInt(3),resultSet.getBoolean(3)); 

			}
		} catch (Exception e) { 
			e.printStackTrace();
		} finally {
			try {
				preparedStatement.close();
				resultSet.close();
				ConnectionFactoryUtils.disconnect();
				conn.close();
			}catch (SQLException e) {
				System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage()); 
			}
			
		}
		
		return storeQueue;
		
	}
	
	public void admitUpdate(final StoreQueue queue)  throws SQLException{
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		try {
			conn = ConnectionFactoryUtils.createConnetion();
			preparedStatement = conn.prepareStatement(SQLCommandsConstants.STORE_QUEUE_UPDATE_ADMITTED);
			preparedStatement.setInt(1, queue.getIdQueue());
			preparedStatement.executeUpdate();
 
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				preparedStatement.close();
				ConnectionFactoryUtils.disconnect();
				conn.close();
			}catch (SQLException e) {
				System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
			}
			
		}
		
	}
	

	@Override
	public StoreQueue add(final StoreQueue entity)  throws SQLException{
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public StoreQueue get(final int idEntity) throws SQLException{
		// TODO Auto-generated method stub
		return null;
	}




}
