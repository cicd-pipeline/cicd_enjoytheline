package com.ait.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.ait.entities.Store;
import com.ait.resources.ConnectionFactoryUtils;
import com.ait.resources.SQLCommandsConstants;

public class StoreDAO implements EntityDAO<Store> {

	@Override
	public Store add(final Store store) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Store get(final int idStore) throws SQLException {
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		Store store = null;
		try {
			try {
				conn = ConnectionFactoryUtils.createConnetion();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			preparedStatement = conn.prepareStatement(SQLCommandsConstants.STORE_SELECT);
			preparedStatement.setInt(1, idStore);
			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				store = new Store(resultSet.getInt(1), resultSet.getString(2), resultSet.getInt(3));

			} 
		} finally {
			try {
				resultSet.close();
				preparedStatement.close();
				ConnectionFactoryUtils.disconnect();
				conn.close();
			} catch (SQLException e) {
				System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
			}
		}
		return store;
	}

	public List<Store> getAll()  throws SQLException{
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		final List<Store> storeList = new ArrayList<Store>();
		try {
			conn = ConnectionFactoryUtils.createConnetion();
			preparedStatement = conn.prepareStatement(SQLCommandsConstants.STORE_SELECT_ALL);
			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				final Store store = new Store(resultSet.getInt(1), resultSet.getString(2), resultSet.getInt(3));
				storeList.add(store);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				resultSet.close();
				preparedStatement.close();
				ConnectionFactoryUtils.disconnect();
				conn.close();
			} catch (SQLException e) {
				System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
			}

		}
		return storeList;
	}

	public void updateCapacity(final Store store, final int capacity)  throws SQLException{
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		try {
			conn = ConnectionFactoryUtils.createConnetion();
			preparedStatement = conn.prepareStatement(SQLCommandsConstants.STORE_UPDATE);
			preparedStatement.setInt(1, capacity);
			preparedStatement.setInt(2, store.getIdStore());
			preparedStatement.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				preparedStatement.close();
				ConnectionFactoryUtils.disconnect();
				conn.close();
			} catch (SQLException e) {
				System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
			}
		}
	}
}
