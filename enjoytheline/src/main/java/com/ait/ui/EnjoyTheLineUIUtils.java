package com.ait.ui;

import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

import com.ait.dao.FactoryDAOUtils;
import com.ait.entities.Person;
import com.ait.entities.Store;
import com.ait.entities.StoreQueue;
import com.ait.exceptions.EnjoyTheLineException;
import com.ait.facade.PersonFacade;
import com.ait.facade.StoreFacade;
import com.ait.facade.StoreQueueFacade;

public class EnjoyTheLineUIUtils {
	// declare two class variables
	private static Scanner scanner = new Scanner(System.in);

	public static void main(final String args[]) throws SQLException, NumberFormatException, EnjoyTheLineException {
		System.out.println("Welcome to the Enjoy the Line Application\n");
		displayMainMenu();
		String action = "";
		boolean loop = true;
		while (loop) {
			// get the input from the user
			action = ValidatorUtils.getString(scanner, "Enter the number option (1-3): ");
			System.out.println();

			if ("1".equalsIgnoreCase(action)) { 
				displayCustomerMenu();
			} else if ("2".equalsIgnoreCase(action)) {
				displayStoreMenu();
			} else if ("3".equalsIgnoreCase(action)) {
				System.out.println("See you soon!!!\n");
				loop = false;
			} else {
				System.out.println("Error! Not a valid command.\n");
				System.out.println("Commands should be between 1-3"); 
			}
		}
		scanner.close();
	}

	public static void displayMainMenu() {
		System.out.println("---------- Main Menu ----------");
		System.out.println("1) Display customer menu");
		System.out.println("2) Display store menu");
		System.out.println("3) Exit the application\n");
	}

	public static void displayCustomerMenu() throws SQLException, NumberFormatException, EnjoyTheLineException {

		String action = "";
		boolean loop = true;
		while (loop) {
			// get the input from the user
			System.out.println("----------  Customer Menu ---------- ");
			System.out.println("1) Join a store line");
			System.out.println("2) Leave the line");
			System.out.println("3) Back to Main ");
			action = ValidatorUtils.getString(scanner, "Enter the option number  (1-3): ");
			System.out.println();

			if ("1".equalsIgnoreCase(action)){
				menuJoinTheLine();

			} else if ("2".equalsIgnoreCase(action)) {
				menuLeaveTheLine();

			} else if ("3".equalsIgnoreCase(action)) {
				displayMainMenu();
				loop = false;
			} else {
				System.out.println("Error! Not a valid command.\n");
				System.out.println("Values should be between 1-3");
			}
		}
	}

	public static void displayStoreMenu() throws NumberFormatException, SQLException, EnjoyTheLineException {

		String action = "";
		while ("5".equalsIgnoreCase(action)) {
			System.out.println("----------  Store Menu ---------- ");
			System.out.println("1) Admit next customer in line");
			System.out.println("2) List all customers at line");
			System.out.println("3) Delete customer data - 5 days");
			System.out.println("4) Update   queue capacity of the store ");
			System.out.println("5) Back to Main");
			action = ValidatorUtils.getString(scanner, "Enter the option number  (1-5): ");
			System.out.println();

			if ("1".equalsIgnoreCase(action)) {
				menuAdmitCustomer();
			} else if ("2".equalsIgnoreCase(action)) {
				menuDisplayLine();
			} else if ("3".equalsIgnoreCase(action)) {
				menuDeleteOldData();
			} else if ("4".equalsIgnoreCase(action)) {
				menuUpdateStoreCapacity();
			} else if ("5".equalsIgnoreCase(action)) {
				displayMainMenu();
			} else {
				System.out.println("Error! Not a valid command.\n");
				System.out.println("Commands should be between 1-5");
			}
		}

	}

	public static void menuJoinTheLine() throws SQLException, NumberFormatException, EnjoyTheLineException {
		final String firstName = ValidatorUtils.getLine(scanner, "Enter the person first name: ");
		final String lastName = ValidatorUtils.getLine(scanner, "Enter person last name: ");
		final String postalCode = ValidatorUtils.getLine(scanner, "Enter person postal code: ");
		final String email = ValidatorUtils.getLine(scanner, "Enter person email: ");
		final String phone = ValidatorUtils.getLine(scanner, "Enter person phone: ");

		final Person person = new Person(0,firstName, lastName, postalCode, email, phone);
		displayAllStores();
		final String idStore = ValidatorUtils.getLine(scanner, "Enter the store id that you want to join: ");
		final Store store = getStore(Integer.parseInt(idStore));

		final StoreQueueFacade queueFacade = new StoreQueueFacade(FactoryDAOUtils.getStoreQueueDAO(),
				FactoryDAOUtils.getStoreDAO(), FactoryDAOUtils.getPersonDAO());
		try {
			queueFacade.joinTheLine(person, store);
		} catch (NumberFormatException e) {
			System.out.println("Invalid id!!");
			return;
		} catch (EnjoyTheLineException e) {
			System.out.println(e.getMessage());
			return;
		}
		System.out.println("Customer added to the queue!!");

	}

	public static void menuLeaveTheLine() throws NumberFormatException, SQLException {
		final String email = ValidatorUtils.getLine(scanner, "Enter customer email: ");
		Person person = null;

		try {
			person = getPersonByEmail(email);
		} catch (EnjoyTheLineException e) {
			e.printStackTrace();
		}
		final StoreQueueFacade storeQueueFacade = new StoreQueueFacade(FactoryDAOUtils.getStoreQueueDAO(),
				FactoryDAOUtils.getStoreDAO(), FactoryDAOUtils.getPersonDAO());
		List<StoreQueue> queues = null;
		try {
			queues = storeQueueFacade.getQueueByName(person);
		} catch (EnjoyTheLineException e) {
			System.out.println(e.getMessage());

		}

		System.out.println("Your Queues : ");

		for (final StoreQueue queue : queues) {
			System.out.println(" ID : " + queue.getIdQueue() + " Store : " + queue.getIdStore());
		}
		final String queueId = ValidatorUtils.getLine(scanner, "Enter the line that you want to leave: ");
		final StoreQueueFacade queueFacade = new StoreQueueFacade(FactoryDAOUtils.getStoreQueueDAO(),
				FactoryDAOUtils.getStoreDAO(), FactoryDAOUtils.getPersonDAO());
		queueFacade.deleteQueue(Integer.parseInt(queueId));
		System.out.println("Removed from the line!");

	}

	public static void menuAdmitCustomer() throws NumberFormatException, SQLException {
		displayAllStores();
		final String storeId = ValidatorUtils.getLine(scanner, "Enter the store id that you want to use: ");

		final StoreQueueFacade queueFacade = new StoreQueueFacade(FactoryDAOUtils.getStoreQueueDAO(),
				FactoryDAOUtils.getStoreDAO(), FactoryDAOUtils.getPersonDAO());
		final Person person = queueFacade.admitToStore(Integer.parseInt(storeId));
		System.out.println("Person Name " + person.getFirstName() + " " + person.getLastName() + " has been admited !");
	}

	public static void menuDisplayLine() throws SQLException, NumberFormatException, EnjoyTheLineException {
		displayAllStores();
		final String storeId = ValidatorUtils.getLine(scanner, "Enter the store id that you want to use: ");
		final Store store = getStore(Integer.parseInt(storeId));
		final StoreQueueFacade queueFacade = new StoreQueueFacade(FactoryDAOUtils.getStoreQueueDAO(),
				FactoryDAOUtils.getStoreDAO(), FactoryDAOUtils.getPersonDAO());

		final List<StoreQueue> queues = queueFacade.getQueueByStore(store);
		if (!queues.isEmpty()) {
			for (final StoreQueue queue : queues) {
				final Person person = getPersonById(queue.getIdPerson());
				System.out.println(" QUEUE ID : " + queue.getIdQueue() + " Name : " + person.getFirstName() + " "
						+ person.getLastName());
			}
		} else {
			System.out.println("No customers at the queue");
		}
	}

	public static void menuDeleteOldData() throws NumberFormatException, SQLException {
		displayAllStores();
		final String storeId = ValidatorUtils.getLine(scanner, "Enter the store id that you want to use: ");
		final StoreQueueFacade storeQueueFacade = new StoreQueueFacade(FactoryDAOUtils.getStoreQueueDAO(),
				FactoryDAOUtils.getStoreDAO(), FactoryDAOUtils.getPersonDAO());
		storeQueueFacade.deleteOldValues(Integer.parseInt(storeId));
		System.out.println("Old queues Values has been deleted from store " + storeId);

	}

	public static void menuUpdateStoreCapacity() throws NumberFormatException, SQLException, EnjoyTheLineException {
		displayAllStores();
		final String storeId = ValidatorUtils.getLine(scanner, "Enter the store id that you want to use: ");
		final Store store = getStore(Integer.parseInt(storeId));
		System.out.println(" The store has a capacity of " + store.getMaxCapacity());
		final String capacity = ValidatorUtils.getLine(scanner, "Enter the new store capacity ");
		final StoreFacade storeFacade = new StoreFacade(FactoryDAOUtils.getStoreDAO());

		storeFacade.updateCapacity(store, Integer.parseInt(capacity));

		System.out.println(" The capacity has been updated");
	}

	public static void displayAllStores() throws SQLException {
		final StoreFacade storeFacade = new StoreFacade(FactoryDAOUtils.getStoreDAO());
		final List<Store> stores = storeFacade.getStores();

		System.out.println("----------  List of Stores  ---------- ");
		for (final Store store : stores) {
			System.out.println("Store ID: " + store.getIdStore() + " Store Name: " + store.getStoreName()
					+ " Max Capacity : " + store.getMaxCapacity());
		}

	}

	public static Person getPersonById(final int idPerson) throws SQLException {
		final PersonFacade personFacade = new PersonFacade(FactoryDAOUtils.getPersonDAO());
		return personFacade.getPerson(idPerson);

	}

	public static Store getStore(final Integer idStore) throws SQLException, EnjoyTheLineException {
		final StoreFacade storeFacade = new StoreFacade(FactoryDAOUtils.getStoreDAO());
		return storeFacade.getStore(idStore);

	}

	public static Person getPersonByEmail(final String email) throws EnjoyTheLineException, SQLException {
		final PersonFacade personFacade = new PersonFacade(FactoryDAOUtils.getPersonDAO());
		return personFacade.getPersonByEmail(email);

	}

}
