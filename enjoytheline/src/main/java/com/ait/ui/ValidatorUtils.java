package com.ait.ui;

import java.util.Scanner;

public class ValidatorUtils
{
    public static String getString(final Scanner scanner, final String prompt)
    {
        System.out.print(prompt);
        final String string = scanner.next();        // read the first string on the line
        scanner.nextLine();               // discard any other data entered on the line
        return string;

    }

    public static String getLine(final Scanner scanner, final String prompt)
    {
        System.out.print(prompt);
        final String string = scanner.nextLine();        // read the whole line
        return string;
    }
 
}
