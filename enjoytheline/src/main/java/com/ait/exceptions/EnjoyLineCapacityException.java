package com.ait.exceptions;

public class EnjoyLineCapacityException extends EnjoyTheLineException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8687261144452365674L;

	public EnjoyLineCapacityException(final String store) {
		super("Sorry, number of customers exceeded at store: " + store);
	}

}
