package com.ait.exceptions;

public class EnjoyLinePersonException extends EnjoyTheLineException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8687261144452365674L;

	public EnjoyLinePersonException(final String personEmail) {
		super("Customer  not found at line: " + personEmail);
	}

} 
