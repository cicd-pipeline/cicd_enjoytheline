package com.ait.exceptions;

public class EnjoyLineNoQueueException extends EnjoyTheLineException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8687261144452365674L;

	public EnjoyLineNoQueueException(final String personEmail) {
		super("No queues found for: " + personEmail);
	}

}
