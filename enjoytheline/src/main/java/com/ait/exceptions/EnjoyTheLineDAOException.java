package com.ait.exceptions;

public class EnjoyTheLineDAOException extends EnjoyTheLineException {

	private static final long serialVersionUID = 334051992916748022L;

	public EnjoyTheLineDAOException() {
		super("Error in connection to EnjoyTheLine database");
	}

}
