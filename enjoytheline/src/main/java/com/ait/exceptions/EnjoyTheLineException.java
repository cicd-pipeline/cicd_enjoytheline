package com.ait.exceptions;

public abstract class EnjoyTheLineException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4370764216328369551L;

	protected EnjoyTheLineException (final String message) {
		super(message);
	}

}
