package com.ait.facade;

import java.sql.SQLException;
import java.util.List;

import com.ait.dao.StoreDAO;
import com.ait.entities.Store;
import com.ait.exceptions.EnjoyTheLineDAOException;
import com.ait.exceptions.EnjoyTheLineException;

public class StoreFacade {

	StoreDAO database;

	public StoreFacade(final StoreDAO dao) {
		database = dao;
	}

	public Store getStore(final int idStore) throws EnjoyTheLineException  {
		try {
			return database.get(idStore);
		} catch (SQLException e) {
		   throw new EnjoyTheLineDAOException();
		}
	}

	public List<Store> getStores() throws SQLException {
		return database.getAll();
 
	}

	public void updateCapacity(final Store store, final int capacity) throws SQLException {

		database.updateCapacity(store, capacity);

	}

}
