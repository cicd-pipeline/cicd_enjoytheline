package com.ait.facade;

import java.sql.SQLException;
import java.util.List;

import com.ait.dao.PersonDAO;
import com.ait.dao.StoreDAO;
import com.ait.dao.StoreQueueDAO;
import com.ait.entities.Person;
import com.ait.entities.Store;
import com.ait.entities.StoreQueue;
import com.ait.exceptions.EnjoyLineCapacityException;
import com.ait.exceptions.EnjoyLineNoQueueException;
import com.ait.exceptions.EnjoyTheLineException;

public class StoreQueueFacade {

	StoreQueueDAO queueDao;
	StoreDAO storeDAO;
	PersonDAO personDAO;

	public StoreQueueFacade(final StoreQueueDAO queueDao, final StoreDAO storeDAO, final PersonDAO personDAO) {
		this.queueDao = queueDao;
		this.storeDAO = storeDAO;
		this.personDAO = personDAO;
	}

	public List<StoreQueue> getQueueByStore(final Store store) throws SQLException {
		return queueDao.getByStore(store);

	}

	public void joinTheLine(final Person person, final Store store) throws EnjoyTheLineException, SQLException {

		final List<StoreQueue> queue = queueDao.getByStore(store);
		int queueSize = 0;
		if (!queue.isEmpty()) {
			queueSize = queue.size();
		}

		if (store.getMaxCapacity() >= queueSize) {
			final Person personQueue = personDAO.add(person);
			queueDao.add(store, personQueue);
		} else {
			throw new EnjoyLineCapacityException(store.getStoreName());
		}

	}

	public void deleteQueue(final int idStoreQueue) throws SQLException {
		queueDao.delete(idStoreQueue);

	}

	public Person admitToStore(final int idStore) throws SQLException {

		final Store store = storeDAO.get(idStore);
		final StoreQueue storeQueue = queueDao.admitNextCustomer(store);
		final Person person = personDAO.get(storeQueue.getIdPerson());
		queueDao.admitUpdate(storeQueue);
		return person;

	}

	public List<StoreQueue> getQueueByName(final Person person) throws EnjoyTheLineException, SQLException {
		final List<StoreQueue> queues = queueDao.getByName(person);
		if (queues.isEmpty()) {
			throw new EnjoyLineNoQueueException(person.getEmail());
		}

		return queues;
	}

	public void deleteOldValues(final int idStoreQueue) throws SQLException {
		queueDao.deleteOldValues(idStoreQueue);
	}

}
