package com.ait.facade;

import java.sql.SQLException;

import com.ait.dao.PersonDAO;
import com.ait.entities.Person;
import com.ait.exceptions.EnjoyLinePersonException;
import com.ait.exceptions.EnjoyTheLineException;

public class PersonFacade {

	PersonDAO database;

	public PersonFacade(final PersonDAO dao) {
		this.database = dao;
	}

	public Person addPerson(final Person person) throws SQLException {
		return  database.add(person);
	}

	public Person getPerson(final int idPerson) throws SQLException {
		return  database.get(idPerson);
	}

	public Person getPersonByEmail(final String email) throws EnjoyTheLineException, SQLException {
		final Person person = database.getByEmail(email);

		if (person == null) {
			throw new EnjoyLinePersonException(email);
		}
		return person;
	}

}
