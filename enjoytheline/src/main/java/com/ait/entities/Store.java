package com.ait.entities;

public class Store {

	private final int idStore;
	private final String storeName;
	private final int maxCapacity;

	public Store(final int idStore, final String storeName, final int maxCapacity) {
		super();
		this.idStore = idStore;
		this.storeName = storeName;
		this.maxCapacity = maxCapacity;

	}

	public int getIdStore() {
		return idStore;
	}

	public String getStoreName() {
		return storeName;
	}

	public int getMaxCapacity() {
		return maxCapacity;
	}

}
