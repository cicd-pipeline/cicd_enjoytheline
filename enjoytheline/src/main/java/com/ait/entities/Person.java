package com.ait.entities;

public class Person {

	private final Integer idPerson;
	private final String firstName;
	private final String lastName;
	private final String phone;
	private final String email;
	private final String postalCode;

	public Person(final int idPerson, final String firstName, final String lastName, final String phone, final String email, final String postalCode) {
		super();
		
	
		this.idPerson = idPerson;
		this.firstName = firstName;
		this.lastName = lastName;
		this.phone = phone;
		this.email = email;
		this.postalCode = postalCode;
	}
	
	

	public Integer getIdPerson() {
		return idPerson;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getPhone() {
		return phone;
	}

	public String getEmail() {
		return email;
	}

	public String getPostalCode() {
		return postalCode;
	}

}
