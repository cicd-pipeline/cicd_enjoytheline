package com.ait.entities;

public class StoreQueue {
	
	private final int idQueue;
	private final int idStore;
	private final int idPerson;
	private final boolean admittedInStore;

	public StoreQueue(final int idQueue, final int idStore, final int idPerson, final boolean admittedInStore) {
		super();
		this.idQueue = idQueue;
		this.idStore = idStore;
		this.idPerson = idPerson;
		this.admittedInStore = admittedInStore; 
	}

	public int getIdQueue() {
		return idQueue;
	}

	public int getIdStore() {
		return idStore;
	}

	public int getIdPerson() {
		return idPerson;
	}

	public boolean isAdmittedInStore() {
		return admittedInStore;
	}


}
