package enjoytheline;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.ait.dao.PersonDAO;
import com.ait.dao.StoreDAO;
import com.ait.dao.StoreQueueDAO;
import com.ait.entities.Person;
import com.ait.entities.Store;
import com.ait.entities.StoreQueue;
import com.ait.exceptions.EnjoyLineCapacityException;
import com.ait.exceptions.EnjoyLineNoQueueException;
import com.ait.exceptions.EnjoyLinePersonException;
import com.ait.exceptions.EnjoyTheLineDAOException;
import com.ait.exceptions.EnjoyTheLineException;
import com.ait.facade.PersonFacade;
import com.ait.facade.StoreFacade;
import com.ait.facade.StoreQueueFacade;

class EnjoyTheLineTest {

	// mocks
	private final PersonDAO personDAO = mock(PersonDAO.class);
	private final StoreDAO storeDAO = mock(StoreDAO.class);
	private final StoreQueueDAO storeQueueDAO = mock(StoreQueueDAO.class);
	private final PersonFacade personFacadeMock = mock(PersonFacade.class);
//	private final   StoreFacade storeFacadeMock = mock(StoreFacade.class);

	private List<StoreQueue> queueList;
	private List<Store> storeList;

	// Objects
	private Store apple, tesco;
	private Person fabi, rafa;

	// constants
	private static final String INVALID_EMAIL = "test@test";

	// facade
	PersonFacade personFacade;
	StoreQueueFacade queueFacade;
	StoreFacade storeFacade;
	StoreQueue queueRafaTesco, queueFabiTesco;

	@BeforeEach
	void setUp() throws Exception {

		apple = new Store(1, "Apple Store", 2);
		tesco = new Store(2, "Tesco Store", 0);

		storeList = new ArrayList<Store>();
		storeList.add(apple);
		storeList.add(tesco);

		fabi = new Person(1, "Fabi", "Velosa", "123456", "fabi@fabi", "54321");
		rafa = new Person(2, "Rafa", "Maeda", "123456", "rafa@rafa", "54321");

		queueRafaTesco = new StoreQueue(1, tesco.getIdStore(), rafa.getIdPerson(), Boolean.FALSE);
		queueFabiTesco = new StoreQueue(2, tesco.getIdStore(), fabi.getIdPerson(), Boolean.FALSE);
		queueList = new ArrayList<StoreQueue>();
		queueList.add(queueRafaTesco);
		queueList.add(queueFabiTesco);

		storeFacade = new StoreFacade(storeDAO);
		personFacade = new PersonFacade(personDAO);
		queueFacade = new StoreQueueFacade(storeQueueDAO, storeDAO, personDAO);
	}

	@Test
	void testPersonNotFound() throws EnjoyTheLineException, SQLException {
		final Throwable exception = assertThrows(EnjoyLinePersonException.class, () -> {
			personFacade.getPersonByEmail(INVALID_EMAIL);
		});

		assertEquals("Customer  not found at line: " + INVALID_EMAIL, exception.getMessage());

		verify(personDAO, times(1)).getByEmail(INVALID_EMAIL);
	}

	@Test
	void testStoreCapacityExceeded() throws EnjoyTheLineException, SQLException {

		when(storeQueueDAO.getByStore(tesco)).thenReturn(queueList);
		when(personFacadeMock.addPerson(rafa)).thenReturn(rafa);
		when(storeDAO.get(tesco.getIdStore())).thenReturn(tesco);
		when(personDAO.add(rafa)).thenReturn(rafa);

		final Throwable exception = assertThrows(EnjoyLineCapacityException.class, () -> {
			queueFacade.joinTheLine(fabi, tesco);
		});

		assertEquals("Sorry, number of customers exceeded at store: " + tesco.getStoreName(), exception.getMessage());
		verify(storeQueueDAO, times(1)).getByStore(tesco);

	}

	@Test
	void testDAOException() throws EnjoyTheLineException, SQLException {

		when(storeDAO.get(tesco.getIdStore())).thenThrow(SQLException.class);
		final Throwable exception = assertThrows(EnjoyTheLineDAOException.class, () -> {
			storeFacade.getStore(tesco.getIdStore());
		});
		assertEquals("Error in connection to EnjoyTheLine database", exception.getMessage());

	}

	@Test
	void testAddPersontoLineSuccess() throws EnjoyTheLineException, SQLException {

		when(storeQueueDAO.getByStore(apple)).thenReturn(queueList);
		when(personFacadeMock.addPerson(rafa)).thenReturn(rafa);
		when(personDAO.add(rafa)).thenReturn(rafa);

		queueFacade.joinTheLine(rafa, apple);
		verify(storeQueueDAO, times(1)).getByStore(apple);
		assertEquals("Rafa", rafa.getFirstName());
		assertEquals("Maeda", rafa.getLastName());
		assertEquals("123456", rafa.getPhone());
		assertEquals("rafa@rafa", rafa.getEmail());
		assertEquals("54321", rafa.getPostalCode());

	}

	@Test
	void testRemovePersonFromLineSuccess() throws EnjoyTheLineException, SQLException {

		when(storeQueueDAO.getByStore(tesco)).thenReturn(queueList);
		when(personFacadeMock.addPerson(rafa)).thenReturn(rafa);
		when(personDAO.add(rafa)).thenReturn(rafa);

		queueFacade.deleteQueue(queueRafaTesco.getIdQueue());
		verify(storeQueueDAO, times(1)).delete(queueRafaTesco.getIdQueue());
		assertEquals(2, queueRafaTesco.getIdStore());

	}

	@Test
	void testNoQueueFound() throws EnjoyTheLineException, SQLException {

		final Throwable exception = assertThrows(EnjoyLineNoQueueException.class, () -> {
			queueFacade.getQueueByName(fabi);
		});

		assertEquals("No queues found for: " + fabi.getEmail(), exception.getMessage());
		verify(storeQueueDAO, times(1)).getByName(fabi); 

	}

	@Test
	void testQueueFoundSuccess() throws EnjoyTheLineException, SQLException {

		when(storeQueueDAO.getByName(fabi)).thenReturn(queueList);

		queueFacade.getQueueByName(fabi);
		verify(storeQueueDAO, times(1)).getByName(fabi);

	}

	@Test
	void testaddPersonSuccess() throws EnjoyTheLineException, SQLException {

		when(personDAO.add(fabi)).thenReturn(fabi);
		when(personDAO.get(fabi.getIdPerson())).thenReturn(fabi);
		when(personDAO.getByEmail(fabi.getEmail())).thenReturn(fabi);


		personFacade.addPerson(fabi);
		verify(personDAO, times(1)).add(fabi);
		assertEquals(fabi, personFacade.getPerson(1));
		assertEquals(fabi, personFacade.getPersonByEmail(fabi.getEmail()));
		

	}

	@Test
	void testAdmitPersonSuccess() throws EnjoyTheLineException, SQLException {

		when(storeQueueDAO.getByStore(tesco)).thenReturn(queueList);
		when(storeQueueDAO.admitNextCustomer(tesco)).thenReturn(queueFabiTesco);
		when(personFacadeMock.addPerson(fabi)).thenReturn(fabi);
		when(storeDAO.get(tesco.getIdStore())).thenReturn(tesco);
		when(personDAO.add(fabi)).thenReturn(fabi);

		queueFacade.getQueueByStore(tesco);
		verify(storeQueueDAO, times(1)).getByStore(tesco);
		assertEquals(2, queueList.size());
		assertTrue(!queueFabiTesco.isAdmittedInStore());
		assertTrue(!queueFabiTesco.isAdmittedInStore());

	}

	@Test
	void testDisplayAllStores() throws EnjoyTheLineException, SQLException {

		when(storeQueueDAO.getByStore(tesco)).thenReturn(queueList);
		when(storeDAO.getAll()).thenReturn(storeList);
		when(storeDAO.get(tesco.getIdStore())).thenReturn(tesco);

		storeFacade.getStores();
		verify(storeDAO, times(1)).getAll();
		assertEquals("Tesco Store", storeFacade.getStore(tesco.getIdStore()).getStoreName());

	}

	@Test
	void testDisplayLineSuccess() throws EnjoyTheLineException, SQLException {

		when(storeQueueDAO.getByStore(tesco)).thenReturn(queueList);
		when(storeQueueDAO.admitNextCustomer(tesco)).thenReturn(queueFabiTesco);
		when(personFacadeMock.addPerson(fabi)).thenReturn(fabi);
		when(storeDAO.get(tesco.getIdStore())).thenReturn(tesco);
		when(personDAO.add(fabi)).thenReturn(fabi);

		queueFacade.admitToStore(tesco.getIdStore());
		verify(storeQueueDAO, times(1)).admitUpdate(queueFabiTesco);

	}

	@Test
	void testDeleteOldDataSuccess() throws EnjoyTheLineException, SQLException {

		when(storeQueueDAO.getByStore(tesco)).thenReturn(queueList);
		when(storeQueueDAO.admitNextCustomer(tesco)).thenReturn(queueFabiTesco);
		when(personFacadeMock.addPerson(fabi)).thenReturn(fabi);
		when(storeDAO.get(tesco.getIdStore())).thenReturn(tesco);
		when(personDAO.add(fabi)).thenReturn(fabi);

		queueFacade.deleteOldValues(tesco.getIdStore());
		verify(storeQueueDAO, times(1)).deleteOldValues(queueFabiTesco.getIdQueue());

	}

	@Test
	void testUpdateQueueCapacitySuccess() throws EnjoyTheLineException, SQLException {

		when(storeDAO.get(tesco.getIdStore())).thenReturn(tesco);

		storeFacade.updateCapacity(tesco, 10);
		verify(storeDAO, times(1)).updateCapacity(tesco, 10);

	}
}
